#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct BOOK *bk;
int maxID = -1, minID = -1;

struct BOOK
{
	char title[256];
	char fio[256];
	int year;
};

void Reading(FILE *fp, struct BOOK *st, int Index)
{
	char buf[256];
	static int max = 0;
	static int min = 2016;
	fgets(st->title, 256, fp);
	fgets(st->fio, 256, fp);
	fgets(buf, 256, fp);
	st->year = atoi(buf);
	if (st->year > max)
	{
		maxID = Index;
		max = st->year;
	}
	if (st->year < min)
	{
		minID = Index;
		min = st->year;
	}
}

void Output(FILE *fp, struct BOOK *st)
{
	printf("%s%s%d\n\n", st->title, st->fio, st->year);
}

void StructArrSort(int size)
{
	int i, j, temp_year;
	char temp_fio[256] = { '\0' };
	char temp_title[256] = { '\0' };
	for (i = 1; i < size; i++)
	{
		j = i;
		strcpy(temp_fio, bk[i].fio);
		strcpy(temp_title, bk[i].title);
		temp_year = bk[i].year;
		while (j > 0 && strcmp(temp_fio, bk[j - 1].fio) < 0)
		{
			strcpy(bk[j].fio, bk[j - 1].fio);
			strcpy(bk[j].title, bk[j - 1].title);
			bk[j].year = bk[j - 1].year;
			j--;
		}
		strcpy(bk[j].fio, temp_fio);
		strcpy(bk[j].title, temp_title);
		bk[j].year = temp_year;
	}
}


int main()
{
	FILE *fp;
	int i = 0, pos, end;
	int memory = 0;
	fp = fopen("input.txt", "r");
	if (fp == NULL)
	{
		perror("input.txt: ");
		return 1;
	}
	fseek(fp, 0, SEEK_END);
	end = ftell(fp);
	rewind(fp);
	pos = ftell(fp);
	while (pos != end)
	{
		bk = (struct BOOK *)realloc(bk, (memory += 1) * sizeof(struct BOOK));
		Reading(fp, bk + i, i);
		Output(fp, bk + i);
		i++;
		pos = ftell(fp);
	}
	if (maxID == -1 || minID == -1)
	{
		puts("Data is not founded");
		return 1;
	}
	else
	{
		printf("\nThe oldest book:\n%s%s%d\n", bk[minID].title, bk[minID].fio, bk[minID].year);
		printf("\nThe newest book:\n%s%s%d\n", bk[maxID].title, bk[maxID].fio, bk[maxID].year);
	}
	StructArrSort(memory);
	puts("\n\nSorted list of books:\n");
	for (i = 0; i < memory; i++)
		Output(fp, bk + i);
	free(bk);
	return 0;
}